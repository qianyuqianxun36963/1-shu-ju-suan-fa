package com.ustc.wang.z_assist.example;


@SuppressWarnings("serial")
public class QueueEmptyException extends RuntimeException {

	public QueueEmptyException(String err) {
		super(err);
	}
}
