package com.ustc.wang.z_assist.example;


@SuppressWarnings("serial")
public class UnsupportedOperation extends RuntimeException {
	public UnsupportedOperation(String err) {
		super(err);
	}
}
