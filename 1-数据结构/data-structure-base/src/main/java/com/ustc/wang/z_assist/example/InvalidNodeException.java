package com.ustc.wang.z_assist.example;


@SuppressWarnings("serial")
public class InvalidNodeException extends RuntimeException {
	public InvalidNodeException(String err) {
		super(err);
	}
}
