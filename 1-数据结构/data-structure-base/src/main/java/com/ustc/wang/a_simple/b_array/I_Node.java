package com.ustc.wang.a_simple.b_array;


public interface I_Node {
	//获取结点数据域
	public Object getData();
	//设置结点数据域
	public void setData(Object obj);
}
